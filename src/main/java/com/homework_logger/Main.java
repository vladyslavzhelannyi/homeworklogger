package com.homework_logger;

import static com.homework_logger.constants.LoggerConstants.SUCCESSFUL_LAUNCH;

import com.homework_logger.exceptions.RandomException;
import com.homework_logger.services.RandomZeroToTen;
import org.apache.log4j.Logger;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Logger mainLogger = Logger.getLogger(Main.class);
        Random random = new Random();
        RandomZeroToTen randomZeroToTen = new RandomZeroToTen(random);

        try{
            int randomNumber = randomZeroToTen.getNumber();
            mainLogger.info(SUCCESSFUL_LAUNCH);
        } catch (RandomException e) {
            mainLogger.info(e.getMessage());
        }
    }
}
