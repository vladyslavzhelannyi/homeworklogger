package com.homework_logger.exceptions;

public class RandomException extends Exception{

    private int number;

    public RandomException(String message, int num) {

        super(message + num);
        number = num;

    }
}
