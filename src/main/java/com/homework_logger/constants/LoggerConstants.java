package com.homework_logger.constants;

public class LoggerConstants {

    public static final String GENERATED_NUMBER = "Сгенерированное число – ";
    public static final String SUCCESSFUL_LAUNCH = "Приложение успешно запущено";

}
