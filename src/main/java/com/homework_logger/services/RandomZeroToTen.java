package com.homework_logger.services;

import static com.homework_logger.constants.LoggerConstants.GENERATED_NUMBER;
import com.homework_logger.exceptions.RandomException;

import java.util.Random;

public class RandomZeroToTen {
    private Random random;

    public RandomZeroToTen(Random random) {
        this.random = random;
    }

    public int getNumber() throws RandomException {
        int number = random.nextInt(11);

        if (number <= 5) {
            throw new RandomException(GENERATED_NUMBER, number);
        }
        else {
            return number;
        }
    }
}
