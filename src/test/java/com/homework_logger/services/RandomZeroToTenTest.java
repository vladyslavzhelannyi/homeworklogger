package com.homework_logger.services;

import com.homework_logger.constants.LoggerConstants;
import com.homework_logger.exceptions.RandomException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.Random;

public class RandomZeroToTenTest {
    Random random = Mockito.mock(Random.class);
    RandomZeroToTen cut = new RandomZeroToTen(random);

    static Arguments[] getNumberTestArgs() {
        return new Arguments[] {
          Arguments.arguments(10, 10),
          Arguments.arguments(8, 8)
        };
    }

    @ParameterizedTest
    @MethodSource("getNumberTestArgs")
    void getNumberTest(int number, int expected) throws RandomException {
        Mockito.when(random.nextInt(11)).thenReturn(number);
        int actual = cut.getNumber();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getNumberExceptionTestArgs() {
        return new Arguments[] {
                Arguments.arguments(1),
                Arguments.arguments(3)
        };
    }

    @ParameterizedTest
    @MethodSource("getNumberExceptionTestArgs")
    void getNumberExceptionTest(int number) throws RandomException {
        Mockito.when(random.nextInt(11)).thenReturn(number);
        Assertions.assertThrows(RandomException.class, () -> {int actual = cut.getNumber();});
    }
}
